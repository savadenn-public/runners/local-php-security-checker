#-----------------------------------------
# Variables
#-----------------------------------------
MKFILE_PATH := $(abspath $(lastword ${MAKEFILE_LIST}))
PROJECT_PATH := $(dir ${MKFILE_PATH})
PROJECT_NAME := $(shell basename ${PROJECT_PATH})
UID=$(shell id -u)
export UID
GID=$(shell id -g)
export GID

#-----------------------------------------
# Help commands
#-----------------------------------------
.PHONY:
.DEFAULT_GOAL := help

help: ## Prints this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' ${MAKEFILE_LIST} | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

DOCKER_IMAGE := registry.gitlab.com/savadenn-public/runners/${PROJECT_NAME}
run: build ## Build and run image
	docker run \
		-it --rm \
		--user="${UID:-0}:${GID:-0}" \
		--network=host \
		-v "$(shell pwd)/composer.lock:/composer.lock" \
		${DOCKER_IMAGE}

build: ## Build image
	docker build -t ${DOCKER_IMAGE} .