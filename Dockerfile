FROM alpine:latest

LABEL project="https://gitlab.com/savadenn-public/runners/local-php-security-checker"

RUN apk update && \
    apk add --no-cache \
    ca-certificates \
    curl \
    git \
    && rm -f /tmp/* /etc/apk/cache/*

RUN curl -s https://api.github.com/repos/fabpot/local-php-security-checker/releases/latest \
    | grep "browser_download_url" \
    | grep linux_amd64 \
    | cut -d '"' -f 4 \
    | xargs wget -O /usr/local/bin/local-php-security-checker \
    && chmod +x /usr/local/bin/local-php-security-checker