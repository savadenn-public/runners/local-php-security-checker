# local-php-security-checker

Dockerized version of https://github.com/fabpot/local-php-security-checker compatible with Gitlab CI

## How to use it ?

In your GitLab CI file :

```yaml
# Search for CVE in composer.lock
cve_dependencies_php:
  stage: 🛡️️ validate
  needs: []
  image: registry.gitlab.com/savadenn-public/runners/local-php-security-checker:latest
  script:
    - local-php-security-checker --format=json > cve_dependencies_php.json
  artifacts:
    expose_as: CVE Dependencies PHP
    when: always
    paths:
      - cve_dependencies_php.json
  allow_failure: true
  rules:
    - exists:
        - composer.lock
```